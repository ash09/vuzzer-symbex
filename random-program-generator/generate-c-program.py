import sys,random
sys.setrecursionlimit(10000)


def main():
	depth = int(sys.argv[1])
	code = "\
#include <stdio.h>\n \
#include <stdlib.h>\n \
#include <string.h>\n \
#include <fcntl.h>\n \
#include <unistd.h>\n \
\n \
int main(int argc, char** argv) {\n \
int f1;\n \
char buf[1024];\n \
f1 = open(argv[1], O_RDONLY);\n \
read(f1,buf,1024);\n \
close(f1);\n"
	code += generateNewIfStmt(depth) + "\nreturn 0;\n}"

	with open("test.c","w") as f:
		f.write(code)

	# generate a random input
	with open("input.txt","w") as f:
		for _ in range(0,1024):
			c = random.randint(65,127)
			f.write(chr(c))


def generateNewIfStmt(depth):
	if depth >= 1:
		buf_index1 = random.randint(0,1024)
		buf_index2 = random.randint(0,1024)
		comp_char = random.randint(65,127)
		n = random.randint(0,100)
		cmp_sym = ""
		if n <= 45: cmp_sym = '<'
		elif n >= 55: cmp_sym = ">"
		else: cmp_sym = "=="

		code = "if (buf[%d] + buf[%d] %s %d) {\nprintf(\"1\");\n %s} else {\nprintf(\"0\");\n %s}\n" % (buf_index1,buf_index2,cmp_sym,comp_char,generateNewIfStmt(depth-1),generateNewIfStmt(depth-1))
		return code
	else:
		return ""



if __name__ == '__main__': main()