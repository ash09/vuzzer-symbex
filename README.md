# Symbolic Execution Tools for Vuzzer

How to use BB-weight-angr?
--------------------------
First of all, make sure that the dependencies are installed on the system. 

``` sudo pip2 install angr angrutils networkx ```

Then, in order to execute it, run the following command:

``` python2 BB-weight-angr.py path/to/binary ```