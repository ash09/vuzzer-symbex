 # Copyright (C) 2017 Ashley Lesdalons
 #
 # Written by Ashley Lesdalons <ashley.lesdalons@etu.univ-grenoble-alpes.fr>
 #
 # ========LICENCE========
 # This script is free software: you can redistribute it and/or modify
 # it under the terms of the  GNU Lesser General Public
 # License as published by the Free Software Foundation; either
 # version 2.1 of the License, or (at your option) any later version.
 #
 # This script is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 # Lesser General Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public
 # License along with this script; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 # ========LICENCE========


from tbse import *

m = TBSE("test/a","{}","test/input.txt",[0,1,2,3],0x4006ff)
m.start()
m.join()
print m.getNewInputFiles()