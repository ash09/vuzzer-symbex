 # Copyright (C) 2017 Ashley Lesdalons
 #
 # Written by Ashley Lesdalons <ashley.lesdalons@etu.univ-grenoble-alpes.fr>
 #
 # ========LICENCE========
 # This script is free software: you can redistribute it and/or modify
 # it under the terms of the  GNU Lesser General Public
 # License as published by the Free Software Foundation; either
 # version 2.1 of the License, or (at your option) any later version.
 #
 # This script is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 # Lesser General Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public
 # License along with this script; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 # ========LICENCE========

import sys,string,os
import threading
import subprocess
import json


class InvalidConfigException(Exception):
	pass

def checkConfig():
	global CONFIG
	with open("TBSE/config.json","r") as f:
		CONFIG = json.load(f)
	if CONFIG["architecture"] != "X86" and CONFIG["architecture"] != "X86_64":
		raise InvalidConfigException
	if not os.path.isdir(CONFIG["moduleDir"]):
		raise InvalidConfigException
	if not os.path.isdir(CONFIG["newInputDir"]):
		os.mkdir(CONFIG["newInputDir"])
	if not os.path.exists(CONFIG["tritonScript"]):
		raise InvalidConfigException
	if not os.path.exists(CONFIG["loadLibFile"]):
		raise InvalidConfigException


# Trace-Based Symbolic Engine
class TBSE (threading.Thread):
	'''
	Find new inputs using trace-base symbolic execution and DTA
	'''
	global CONFIG
	checkConfig()

	def __init__(self,binaryPath,binaryArgs,inputPath,symbolicOffsets,cmpAddress):
		super(TBSE,self).__init__()
		self.setParameters(binaryPath,inputPath,symbolicOffsets,cmpAddress)
		self.binaryArgs = binaryArgs.format(self.params["inputPath"])

	def setParameters(self,binaryPath,inputPath,symbolicOffsets,cmpAddress):
		'''
		Arguments:  binaryPath: 		/path/to/the/binary
					binaryArgs: 		arguments of the analyzed binary. '{}' is replaced by the inputPath
										Example: '-f {} -t 2 -h'
					inputPath:  		/path/to/the/original/input
										This input must allow the program to reach the targeted cmp
					symbolicOffsets: 	Offsets of the bytes in the input we want to make symbolic
					cmpAddress:			address of the targeted cmp instruction
		'''
		cmpImagePath,cmpOffset = self.getCmpImagePathAndOffset(cmpAddress)
		self.params = dict()
		self.params["binaryPath"] = os.path.abspath(binaryPath)
		self.params["inputPath"] = os.path.abspath(inputPath)
		self.params["symbolicOffsets"] = symbolicOffsets
		self.params["cmpImagePath"] = os.path.abspath(cmpImagePath)
		self.params["cmpOffset"] = cmpOffset

	def writeParameters(self):
		with open("%s/pin_script.in" % CONFIG["moduleDir"],"w") as f:
			f.write(str(self.params))

	def run(self):
		self.writeParameters()
		os.chdir(CONFIG["moduleDir"])
		cmd = []
		cmd.append(CONFIG["tritonScript"])
		cmd.append("./pin_script.py")
		cmd.append(self.params['binaryPath'])
		cmd.append(self.binaryArgs)
		print("[+] Started to run %s" % cmd)
		proc = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = proc.communicate()
		print(stdout)
		for line in stdout.split("\n"):
			if line.startswith("[out]"):
				l = line[6:]
				self.newInputFiles = map(lambda x: CONFIG["prefixInputFiles"]+x,l.split(","))
				self.removeEmptyFiles()
				return
		# if we are here, an error occured
		print("ERROR:\n%s" % stderr)
		os.chdir("../")

	def getNewInputFiles(self):
		return self.newInputFiles

	def removeEmptyFiles(self):
		for f in self.newInputFiles:
			if f == "None":
				self.newInputFiles.remove(f)

	def getCmpImagePathAndOffset(self,cmpAddress):
		with open(CONFIG["loadLibFile"],'r') as f:
			for line in f:
				r1 = line.split(":")
				lib = r1[0].strip()
				r2 = r1[1].split("-")
				start = int(r2[0].strip(),16)
				end = int(r2[1].strip(),16)
				if cmpAddress >= start and cmpAddress <= end:
					return lib,(cmpAddress-start)
		# if cmpAddress is in none of the ranges
		raise Exception("0x%x isn't in any of the ranges in the loadLibFile." % cmpAddress)