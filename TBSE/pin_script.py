#!/usr/bin/python

 # Copyright (C) 2017 Ashley Lesdalons
 #
 # Written by Ashley Lesdalons <ashley.lesdalons@etu.univ-grenoble-alpes.fr>
 #
 # ========LICENCE========
 # This script is free software: you can redistribute it and/or modify
 # it under the terms of the  GNU Lesser General Public
 # License as published by the Free Software Foundation; either
 # version 2.1 of the License, or (at your option) any later version.
 #
 # This script is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 # Lesser General Public License for more details.
 #
 # You should have received a copy of the GNU Lesser General Public
 # License along with this script; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 # ========LICENCE========

import sys,string,uuid,os,json
from triton import *
from pintool import *


# Read config file
with open("config.json","r") as f:
    CONFIG = json.load(f)

# script parameters
inputPath = None        # path of the input file of the binary
symbolicOffsets = []    # offsets of the bytes in the input we want to make symbolic
cmpImagePath = None     # path of the image containing the targeted cmp instruction
cmpOffset = None        # offset of the cmp instruction
cmpAddress = None       # the address is computed from the 2 previous variables

# some global variables
inputFd   = None
inputAddr = None
inputSize = None
inputRead = 0
isOpen    = False
isRead    = False
nextCmp   = False


def main():
    if CONFIG["architecture"] == "X86":
        setArchitecture(ARCH.X86)
    elif CONFIG["architecture"] == "X86_64":
        setArchitecture(ARCH.X86_64)
    enableTaintEngine(False)
    enableMode(MODE.ALIGNED_MEMORY, True)
    enableMode(MODE.ONLY_ON_SYMBOLIZED, True)

    startAnalysisFromEntry()
    insertCall(syscallsEntry, INSERT_POINT.SYSCALL_ENTRY)
    insertCall(syscallsExit, INSERT_POINT.SYSCALL_EXIT)
    insertCall(analyseCmp, INSERT_POINT.AFTER)
    insertCall(imageLoad, INSERT_POINT.IMAGE_LOAD)

    readParameters()
    runProgram()


def readParameters():
    '''
    This script gets its parameters from the pin_script.in file, stored in the dict format.
    '''
    global inputPath,symbolicOffsets,cmpImagePath,cmpOffset,maxInputs
    with open("pin_script.in","r") as f:
        params = eval(f.read())
        inputPath = params['inputPath']
        symbolicOffsets = set(params['symbolicOffsets'])
        cmpImagePath = params['cmpImagePath']
        cmpOffset = params['cmpOffset']


def getMemoryString(addr,size):
    index = 0
    s = str()
    for index in range(size):
        c = chr(getCurrentMemoryValue(addr+index))
        s += ("" if c not in string.printable else c)
    return s


def generateNewInput(model):  
    global inputAddr,inputSize,symbolicOffsets
    print("[+] Generating new input")
    s = [-1]*inputSize
    for k,v in model.items():
        s[symbolicOffsets[k]] = chr(v.getValue())
    for i in range(inputSize):
        if s[i] == -1:
            s[i] = chr(getCurrentMemoryValue(inputAddr+i))
    return s


def syscallsEntry(threadId, std):
    global isOpen,inputFd,inputPath,isRead
    if getSyscallNumber(std) == SYSCALL.OPEN:
        name = getMemoryString(getSyscallArgument(std, 0),len(inputPath))
        if os.path.abspath(name) == inputPath:
            isOpen = True
    elif getSyscallNumber(std) == SYSCALL.READ:
        fd = getSyscallArgument(std, 0)
        if fd == inputFd:
            isRead = True


def syscallsExit(threadId, std):
    global isOpen,inputFd,symbolicOffsets,isRead,inputAddr,inputSize,inputRead
    if isOpen:
        inputFd = getSyscallReturn(std)
        isOpen = False
    elif isRead:
        inputAddr = getSyscallArgument(std, 1)
        inputSize = getSyscallArgument(std, 2)
        symbolicOffsets2 = set(symbolicOffsets)
        for index in symbolicOffsets2:
            t = index - inputRead
            if t >= 0 and t < inputSize:
                print("[+] Symbolizing input")
                symbolicOffsets.remove(index)
                mem = MemoryAccess(inputAddr+t,CPUSIZE.BYTE,getCurrentMemoryValue(inputAddr+t))
                convertMemoryToSymbolicVariable(mem)
            inputRead += inputSize
        isRead = False

def getSymbolicExprOfOperand(op):
    '''
    Get the symbolic expression of an operand.
    Example: (bvxor ((_ extract 63 0) (_ bv12345 64)) ((_ extract 63 0) (_ bv67890 64)))
    '''
    if op.getType() == OPERAND.MEM:
        op_id = getSymbolicMemoryId(op.getAddress())
        return getFullAstFromId(op_id)
    elif op.getType() == OPERAND.IMM:
        return int(op.getValue())
    elif op.getType() == OPERAND.REG:
        op_id = getSymbolicRegisterId(op)
        return getFullAstFromId(op_id)
    else:
        # problem in the binary ?
        raise Exception("Invalid Operand")

def getModelFromExpr(expr1,expr2,op,pc):
    print("[+] Getting model from symbolic expression for %s" % op)
    if op == '<':
        c = ast.assert_(ast.land(pc,expr1 < expr2))
    elif op == '=':
        c = ast.assert_(ast.land(pc,expr1 == expr2))
    elif op == '>':
        c = ast.assert_(ast.land(pc,expr1 > expr2))
    m = getModel(c)
    if not m:
        return None
    else:
        return generateNewInput(m)

def writeNewInputToFile(newInput,filename):
    with open(filename,'w') as f:
        for i in newInput:
            f.write("%s" % i)


def analyseCmp(inst):
    '''
    Once the targeted CMP is reached, we retrieve the symbolic expression of the operands and 
    compute models for op1 < op2, op1 = op2 and op1 > op2.
    The new inputs found are written to files.
    '''
    global cmpAddress,nextCmp
    if inst.getAddress() == cmpAddress:
        print("[+] Reached targeted CMP")
        print inst
        nextCmp = True
        return
    if nextCmp:
        op1 = inst.getFirstOperand()
        op2 = inst.getSecondOperand()
        op1_expr = getSymbolicExprOfOperand(op1)
        op2_expr = getSymbolicExprOfOperand(op2)
        pc = getPathConstraintsAst()
        newInput = []
        newInput.append( getModelFromExpr(op1_expr,op2_expr,'<',pc) )
        newInput.append( getModelFromExpr(op1_expr,op2_expr,'=',pc) )
        newInput.append( getModelFromExpr(op1_expr,op2_expr,'>',pc) )
        newInputFiles = []
        for _ in range(3):
            newInputFiles.append(str(uuid.uuid4()))
        if newInput[0] is not None:
            writeNewInputToFile(newInput[0],"%s/%s" % (CONFIG["newInputDir"],CONFIG["prefixInputFiles"]+newInputFiles[0]))
        else:
            newInputFiles[0] = "None"
        if newInput[1] is not None:
            writeNewInputToFile(newInput[1],"%s/%s" % (CONFIG["newInputDir"],CONFIG["prefixInputFiles"]+newInputFiles[1]))
        else:
            newInputFiles[1] = "None"
        if newInput[2] is not None:
            writeNewInputToFile(newInput[2],"%s/%s" % (CONFIG["newInputDir"],CONFIG["prefixInputFiles"]+newInputFiles[2]))
        else:
            newInputFiles[2] = "None"
        print "[out] " + ",".join(newInputFiles)
        os.remove("%s/pin_script.in" % CONFIG["moduleDir"]) # cleaning input file
        nextCmp = False


def imageLoad(imagePath,imageBase,imageSize):
    '''
    Computes the CMP address.
    cmpAddress = baseAddress_of_library_or_binary + cmpOffset 
    '''
    global cmpAddress,cmpImagePath,cmpOffset
    if cmpAddress is None and imagePath == cmpImagePath: 
	    cmpAddress =  imageBase + cmpOffset
	    print "[+] CMP address: 0x%x" % cmpAddress
	    stopAnalysisFromAddress(cmpAddress+32)



if __name__ == '__main__':
    main()
