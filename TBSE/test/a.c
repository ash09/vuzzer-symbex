#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char** argv) {
    char buf[10] = {0};
    int f;
    f = open(argv[1], O_RDONLY);
    read(f,buf,10);
    close(f);
    int c,d,e;
    c = buf[0];
    d = buf[3];
    e = c + d;
    
    if (c == 50 && e == 100) printf("Y\n");
    else printf("N\n");
    return 0;
}
